package com.kytms.shipmentTemplate.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 运单模板DAO
 *
 * @author 陈小龙
 * @create 2018-04-09
 */
public interface TemplateDao<ShipmentTemplate>  extends BaseDao<ShipmentTemplate> {
}
